﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Assignment6EF.Models;

namespace EFCodeFirst.Models
{
    public class Franchise
    {
        public int Id { get; set; }
        [MaxLength(150)]
        public string? Name { get; set; }
        public string? Description { get; set; }
        public ICollection<Movie>? Movies { get; set; }
    }
}
