﻿using EFCodeFirst.Models;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment6EF.Models
{
    internal class MovieCharactersDbContext : DbContext
    {
        public DbSet<Character>? Characters { get; set; }
        public DbSet<Movie>? Movies { get; set; }
        public DbSet<Franchise>? Franchises { get; set; }


        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {

            SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();

            builder.DataSource = @"N-SE-01-9952\SQLEXPRESS";
            builder.InitialCatalog = "CodeFirstAssignment6";
            builder.IntegratedSecurity = true;
            builder.Encrypt = false;

            optionsBuilder.UseSqlServer(builder.ConnectionString);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {

            modelBuilder.Entity<Movie>()
                .HasData(new Movie
                {
                    Id = 1,
                    MovieTitle = "The Fellowship of the Ring",
                    Genre = "Fantasy/Adventure",
                    ReleaseYear = 2001,
                    Director = "Peter Jackson",
                    Picture = "https://hbomax-images.warnermediacdn.com/images/GXdu2ZAglVJuAuwEAADbA/tileburnedin?size=1280x720&partner=hbomaxcom&v=44a146b107d9a08f4ea585296a2e811a&host=art-gallery.api.hbo.com&language=sv-se&w=1280",
                    Trailer = "https://www.youtube.com/watch?v=xkiiEjz2O40",
                    FranchiseId = 1
                });

            modelBuilder.Entity<Movie>()
                .HasData(new Movie
                {
                    Id = 2,
                    MovieTitle = "The Two Towers",
                    Genre = "Fantasy/Adventure",
                    ReleaseYear = 2002,
                    Director = "Peter Jackson",
                    Picture = "https://upload.wikimedia.org/wikipedia/en/d/d0/Lord_of_the_Rings_-_The_Two_Towers_%282002%29.jpg",
                    Trailer = "https://www.youtube.com/watch?v=EWwNDEMD3wM",
                    FranchiseId = 1
                });

            modelBuilder.Entity<Movie>()
                .HasData(new Movie
                {
                    Id = 3,
                    MovieTitle = "Harry Potter and the Philosopher's Stone",
                    Genre = "Fantasy/Adventure",
                    ReleaseYear = 2001,
                    Director = "Chris Columbus",
                    Picture = "https://upload.wikimedia.org/wikipedia/en/7/7a/Harry_Potter_and_the_Philosopher%27s_Stone_banner.jpg",
                    Trailer = "https://www.youtube.com/watch?v=VyHV0BRtdxo",
                    FranchiseId = 2
                });

            //one-to-many
            modelBuilder.Entity<Movie>(
            entity =>
            {
                entity.HasOne(d => d.Franchise)
                    .WithMany(p => p.Movies)
                    .HasForeignKey("FranchiseId");
            });

            modelBuilder.Entity<Character>()
                .HasData(new Character
                {
                    Id = 1,
                    FullName = "Smeagol",
                    Alias = "Gollum",
                    Gender = "Male",
                    Picture = "https://static.wikia.nocookie.net/lotr/images/e/e1/Gollum_Render.png/revision/latest?cb=20141218075509"
                });

            modelBuilder.Entity<Character>()
                .HasData(new Character
                {
                    Id = 2,
                    FullName = "Harry Potter",
                    Alias = "The boy who lived",
                    Gender = "Male",
                    Picture = "https://static.independent.co.uk/s3fs-public/thumbnails/image/2016/09/29/15/hp.jpg?quality=75&width=982&height=726&auto=webp"
                });

            modelBuilder.Entity<Franchise>()
                .HasData(new Franchise
                {
                    Id = 1,
                    Name = "The Lord of the Rings",
                    Description = "Set in the fictional world of Middle-earth, the films follow the hobbit Frodo Baggins as he and the Fellowship embark on a quest to destroy the One Ring, to ensure the destruction of its maker, the Dark Lord Sauron."
                });

            modelBuilder.Entity<Franchise>()
                .HasData(new Franchise
                {
                    Id = 2,
                    Name = "Harry Potter",
                    Description = "Harry begins his first year at Hogwarts School of Witchcraft and Wizardry and learns about magic. During the year, Harry and his friends Ron Weasley and Hermione Granger become entangled in the mystery of the Philosopher's Stone which is being kept within the school."
                });
            //many-to-many
            modelBuilder.Entity<Character>()
                .HasMany(p => p.Movies)
                .WithMany(m => m.Characters)
                .UsingEntity<Dictionary<string, object>>(
                    "CharacterMovie",
                    r => r.HasOne<Movie>().WithMany().HasForeignKey("MovieId"),
                    l => l.HasOne<Character>().WithMany().HasForeignKey("CharacterId"),
                    je =>
                    {
                        je.HasKey("CharacterId", "MovieId");
                        je.HasData(
                            new { CharacterId = 1, MovieId = 1 },
                            new { CharacterId = 1, MovieId = 2 },
                            new { CharacterId = 2, MovieId = 3 }
                        );
                    });

        }
    }
}
